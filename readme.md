Notes on the repo (by Sai Gollapudi):

+ Repo is for SSS-Renderer component of the SSS-Framework (which also consists of SSS-Maker and SSS-Persistence or Database or Store)
+ Repo has multiple branches because multiple projects were incrementally /sequentially done. 
+ Early branches had direct code online. This was stored in the code folder with app1.py, app2.py type names
+ Subsequently, literate style of development / documentation was used. All the early codes were subsumed into the Index.Org file which can be untangled into code.
+ SSS-render works with database and also the ANTLR4 parser / lexar code
+ The theory is that a headless browser is invoked in the backend (server) by Python / Selinium code
+ At times two headless browsers are invoked (one to build the target and the other to fetch the content for building). These are under self.driver1, and driver2 variables. Also, typically the source page (driver1) is the one that is being constructued so there is no need for driver2. 
+ Driver2 is used when there is a fetch content from elsewhere type command
+ JavaScript routines do web page transformatins. They play with the DOM. There are two types of these JS routines: Utitlity (the reusable, generic routines), and task specific operations (like adding a Tool Tip or Youtube link etc.)
+ JavaScript routines are injected into the web page through selinium. 
+ When our JS is injected into some arbitrary web page, then through this JS routines we can manipulate the page.
+ The user's requested web page URL is given to backend server. The server opens a Headless Browser using selinium. Fetches the source of the web page. Gets commands for transformation from SSS (which is internally stored as JSON object). The commands are then turned into calls to JS routines through DSL code (which uses ANTLR4 lexar, parser code) on web page that is opened in the headless browser. The operaions are sequenced untill all are complete. Once done, the HTML source code of the transformed page is delivered for viewing.
+ one of the js libraries converts the internal links in the wep page to something that can be opened in the Headless Browser. The URLs are modified so they don't break in the Headless browsers.
+ 

